/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Text,
  StatusBar,
  TextInput,
} from 'react-native';

import {Header, Colors} from 'react-native/Libraries/NewAppScreen';

const UselessTextInput = (props) => {
  return (
    <TextInput
      {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
      editable
      maxLength={40}
    />
  );
};

const App: () => React$Node = () => {
  const [text, onChangeText] = React.useState('Useless Text');
  const [number, onChangeNumber] = React.useState(null);
  const [value, onChangeValue] = React.useState(
    'Useless Multiline Placeholder',
  );

  const handleOnChangeText = (value) => {
    onChangeText(value);
  };

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <Header />
          <Text>Hello world</Text>
          <TextInput
            testID={'messageText'}
            style={styles.input}
            onChangeText={(value) => handleOnChangeText(value)}
            value={text}
          />
          <TextInput
            style={styles.input}
            onChangeText={onChangeNumber}
            value={number}
            placeholder="useless placeholder"
            keyboardType="numeric"
          />
          <UselessTextInput
            multiline
            numberOfLines={4}
            onChangeText={(text) => onChangeValue(text)}
            value={value}
          />
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
  },
});

export default App;

// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow strict-local
//  */

// import React from 'react';
// import {connect, Provider} from 'react-redux';
// // import store from './src/redux/store'
// import redux from './src/redux/store';
// import {SafeAreaProvider} from 'react-native-safe-area-context';
// import {Text} from 'react-native';
// import {responsiveFont} from './src/Themes/Metrics';
// import AppNavigation from './src/navigation/AppNavigation';

// console.disableYellowBox = true;
// Text.defaultProps = Text.defaultProps || {};
// Text.defaultProps.style = {
//   fontSize: responsiveFont(14),
// };
// const App: () => React$Node = () => {
//   return (
//     <SafeAreaProvider>
//       <AppNavigation />
//     </SafeAreaProvider>
//   );
// };
// const ConnectedApp = connect()(App);

// export default function provider() {
//   return (
//     <Provider store={redux.store}>
//       <ConnectedApp />
//     </Provider>
//   );
// }
