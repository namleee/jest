// __tests__/Intro-test.js
import React from 'react';
import renderer from 'react-test-renderer';
import Welcome from '../../Jest/Welcome';

test('renders correctly', () => {
  const tree = renderer.create(<Welcome />).toJSON();
  expect(tree).toMatchSnapshot();
});
