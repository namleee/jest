// __tests__/Intro-test.js
import {shallow} from 'enzyme';
import React from 'react';
import List from '../../Jest/List';

test('renders correctly', () => {
  const tree = shallow(<List />);
  expect(tree).toMatchSnapshot();
});
