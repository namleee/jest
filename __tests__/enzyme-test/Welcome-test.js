// __tests__/Intro-test.js
import {shallow} from 'enzyme';
import React from 'react';
import Welcome from '../../Jest/Welcome';

test('renders correctly', () => {
  const tree = shallow(<Welcome />);
  expect(tree).toMatchSnapshot();
});
