// __tests__/Intro-test.js
import {shallow, mount} from 'enzyme';
import React from 'react';
import {Text, TouchableOpacity, TextInput} from 'react-native';
import List from '../../Jest/List';
import Block from '../../Jest/Block';
import Members from '../../Jest/Members';

const members = [
  {id: 1, name: 'Daphne'},
  {id: 2, name: 'Margret'},
];

test('renders correctly', () => {
  const tree = shallow(<List />);
  expect(tree).toMatchSnapshot();
});

//Basic ways
test('using enzyme in basic ways', () => {
  const title = 'Blah';
  const wrapper = shallow(
    <Block title={title} onPress={() => console.log('hello')} />,
  );
  expect(wrapper.length).toEqual(1);
  expect(wrapper.contains(<Text>{title}</Text>)).toEqual(false);
});

describe('test touchable', () => {
  const mockFunc = jest.fn();
  mockFunc.mockReturnValue('Link on press invoked');
  const wrapper = shallow(<Block title={'hehe'} onPress={mockFunc} />);
  // wrapper.find(TouchableOpacity).first().props().onPress();
  //wrapper.find(TouchableOpacity).first().simulate('press');
  // wrapper.simulate('press');
  // wrapper.find('TouchableOpacity').first().props().onPress();
  // expect(mockFunc.mock.calls.length).toBe(1);
  const addTodoButton = wrapper.find(TouchableOpacity).first();
  // const a = wrapper.findWhere(
  //   (x) => x.text() === 'Press Here' && x.type() === 'TouchableOpacity',
  // );
  // a.props().onPress();
  addTodoButton.props().onPress();
});

it('should have correct props', () => {
  const title = title;
  expect(
    shallow(<Block title={title} />)
      .find(Text) // Use selector to get certain children
      .first() // Get the first child
      .props().data, // Get its props
  ).toEqual(title);
});

it('should render all item in members', () => {
  const wrapper = shallow(<Members members={members} />);

  expect(wrapper.find({testID: 'memberDetail'}).length).toBe(2);
});

it('should render correct names', () => {
  const wrapper = shallow(<Members members={members} />);

  wrapper.find({testID: 'memberDetail'}).forEach((node, index) => {
    expect(node.props().children).toBe(members[index].name);
  });
});

it('test textInput', () => {
  const onSearch = jest.fn();
  const wrapper = shallow(<Block onSearch={onSearch} />);
  wrapper.find(TextInput).simulate('changeText', 'text search text');
  expect(onSearch).toHaveBeenCalledWith('text search text');
  expect(wrapper.find(Text).contains('text search text')).toBe(true);
});
