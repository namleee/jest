// /**
//  * @format
//  */

// import {shallow} from 'enzyme';
// import {render, fireEvent} from '@testing-library/react-native';
// import 'react-native';
// import React from 'react';
// import App from '../App';

// // Note: test renderer must be required after react-native.
// import renderer from 'react-test-renderer';

// // it('renders correctly 1', () => {
// //   renderer.create(<App />);
// // });

// test('renders correctly', () => {
//   const tree = renderer.create(<App />).toJSON();
//   expect(tree).toMatchSnapshot();
// });

// const event = {
//   target: {
//     value: 'This is just for test',
//   },
// };

// it('It should show hello world text', () => {
//   const onChange = jest.fn();

//   const wrapper = shallow(<App />);
//   expect(wrapper.find('Text')).toHaveLength(1);
//   // wrapper.find('Text').simulate('change', event);
//   // expect(onChange).toHaveBeenCalledWith('Hello World');
// });

// describe('NewMessageForm', () => {
//   describe('clicking send', () => {
//     it('clears the message field', () => {
//       const {getByTestId} = render(<App />);

//       fireEvent.changeText(getByTestId('messageText'), 'Hello world');
//       // fireEvent.press(getByTestId('sendButton'));
//       expect(getByTestId('messageText').props.value).toEqual('Hello world');
//     });
//   });
// });
