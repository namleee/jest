/*
  Test with custom components
*/

import React from 'react';
import {
  StyleSheet,
  TouchableHighlight,
  View,
  Text,
  TextInput,
} from 'react-native';

const AnotherItem = (props) => {
  const [text, onChangeText] = React.useState('Useless Text');
  const {description, id, name, stargazers_count} = props;
  const itemStyle = props.isSelected
    ? [styles.item, styles.selected]
    : styles.item;
  return (
    <TouchableHighlight
      onPress={() => {
        props.selectRepo(id);
      }}
      underlayColor="#E0F2F1">
      <View style={itemStyle}>
        <Text style={styles.title}>{name}</Text>
        <Text style={styles.stars}>{`${stargazers_count} stars`}</Text>
        {props.isSelected ? <Text>{description}</Text> : null}
        <TextInput
          testID={'messageText'}
          style={styles.input}
          onChangeText={(value) => onChangeText(value)}
          value={text}
        />
      </View>
    </TouchableHighlight>
  );
};

export const styles = StyleSheet.create({
  item: {
    paddingVertical: 8,
    paddingHorizontal: 24,
  },
  selected: {
    backgroundColor: '#B2DFDB',
  },
  stars: {
    paddingBottom: 8,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingBottom: 4,
  },
});

export default AnotherItem;
