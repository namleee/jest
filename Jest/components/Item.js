import React from 'react';
import {Text, View} from 'react-native';

const TestItem = ({number = 0}) => (
  <View
    style={{
      backgroundColor: 'red',
      height: 50,
      width: 50,
      alignItems: 'center',
      justifyContent: 'center',
    }}>
    <Text>This is My test Test item {number}</Text>
  </View>
);

export default TestItem;
