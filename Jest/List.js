import React from 'react';
import {Text, View, ScrollView} from 'react-native';
import TestItem from './components/Item';
import AnotherItem from './components/AnotherItem';

const TYPES_NUMBER = [1, 2, 3, 4, 5];

const Test = ({params}) => (
  <View>
    <Text>Hello World</Text>
    <Text>This is my first Jest test APP</Text>
    {TYPES_NUMBER.map((key, item) => (
      <TestItem key={key} number={item} />
    ))}
    <AnotherItem
      name={'Hello'}
      description={'abc'}
      id={1}
      stargazers_count={1}
      isSelected
    />
    <AnotherItem
      name={'Hello'}
      description={'abc'}
      id={1}
      stargazers_count={1}
    />
  </View>
);

export default Test;
