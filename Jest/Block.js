import React, {useState} from 'react';
import {Text, View, TouchableOpacity, TextInput} from 'react-native';

const Block = ({title, onPress, onSearch}) => {
  const [text, setText] = useState('');
  return (
    <View>
      <Text data={title}>{title}</Text>
      <TouchableOpacity onPress={onPress}>
        <Text>Press Here</Text>
      </TouchableOpacity>
      <TextInput
        onChangeText={(value) => {
          onSearch(value);
          setText(value);
        }}
      />
      <Text>{text}</Text>
    </View>
  );
};

export default Block;
