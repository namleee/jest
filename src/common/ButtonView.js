/**
 * Created by Hong HP on 3/10/20.
 */
import {TouchableOpacity, StyleSheet, Text} from 'react-native';
import React from 'react';
import {
  isIOS,
  responsiveFont,
  responsiveHeight,
  responsiveWidth,
} from '../Themes/Metrics';
import Colors from '../Themes/Colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default function ButtonView({
  title,
  style,
  onPress,
  disabled,
  hasIcon,
  textStyle,
  children,
  wrapper,
  leftIcon,
  testID,
}) {
  return (
    <TouchableOpacity
      testID={testID}
      style={[
        styles.container,
        style,
        disabled && {backgroundColor: '#CACFD3'},
      ]}
      disabled={disabled}
      onPress={onPress}>
      {!!leftIcon && leftIcon}
      {title && (
        <Text
          style={[
            styles.textStyle,
            !hasIcon && {textAlign: 'center'},
            textStyle,
          ]}>
          {title}
        </Text>
      )}
      {hasIcon && (
        <MaterialIcons name={'arrow-forward'} color={Colors.white} size={25} />
      )}
      {children}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    minHeight: 35,
    height: responsiveHeight(40),
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    backgroundColor: Colors.primary,
    flexDirection: 'row',
  },
  wrapper: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: responsiveWidth(16),
  },
  textStyle: {
    color: Colors.black,
    fontSize: responsiveFont(16),
    flex: 1,
  },
});
