/**
 * Created by Hong HP on 3/10/20.
 */

import React, {useState} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Colors from '../Themes/Colors';
import {
  responsiveFont,
  responsiveHeight,
  responsiveWidth,
} from '../Themes/Metrics';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Fonts from '../Themes/Fonts';

export default function TextInputView({
  value,
  onChangeText,
  title,
  secureTextEntry,
  placeholder,
  onSubmitEditing,
  style,
  titleStyle,
  isRequire,
  editable = true,
  inputStyle,
  multiline = false,
  rightIcon,
  messageError,
  textInputStyle,
  keyboardType,
  onPress,
  maxLength,
  refs,
  onFocus,
  onBlur,
  leftIcon,
  messageInfo,
  countryCode,
  autoFocus,
  testID,
}) {
  const [showPassword, setShowPassword] = useState(false);
  const [isFocus, setFocus] = useState(false);

  return (
    <TouchableOpacity
      style={style}
      onPress={onPress}
      disabled={!onPress}
      activeOpacity={1}>
      {!!title && (
        <Text style={[styles.textStyle, titleStyle]}>
          {title}
          {isRequire && <Text style={{color: Colors.red}}> *</Text>}
        </Text>
      )}
      <View
        style={[
          styles.textInputContainer,
          inputStyle,
          messageError && {borderColor: Colors.red},
        ]}
        pointerEvents={editable ? 'auto' : 'none'}>
        <TextInput
          testID={testID}
          autoFocus={autoFocus}
          value={value}
          ref={refs}
          style={[
            styles.textInput,
            textInputStyle,
            editable && {color: Colors.white},
          ]}
          onChangeText={(text) => {
            onChangeText && onChangeText(text);
          }}
          maxLength={maxLength}
          placeholder={placeholder}
          placeholderTextColor={Colors.gray}
          keyboardType={keyboardType}
          underlineColorAndroid="transparent"
          secureTextEntry={secureTextEntry && !showPassword}
          onSubmitEditing={() => {
            if (onSubmitEditing) {
              onSubmitEditing();
            }
            setFocus(false);
          }}
          onBlur={() => {
            setFocus(false);
            !!onBlur && onBlur();
          }}
          autoCapitalize="none"
          editable={editable && !onPress}
          scrollEnabled={false}
          multiline={multiline}
          textAlignVertical={'top'}
          onFocus={() => {
            setFocus(true);
            !!onFocus && onFocus();
          }}
        />
        {isFocus && value?.length > 0 && (
          <TouchableOpacity
            onPress={() => {
              onChangeText('');
            }}
            style={{marginRight: 10}}>
            <MaterialCommunityIcons
              name={'close-circle'}
              color={Colors.grayLight}
              size={18}
            />
          </TouchableOpacity>
        )}
        {!!rightIcon && rightIcon}
      </View>
      {!!messageError && (
        <Text
          style={{
            fontSize: responsiveFont(11),
            color: Colors.red,
            fontFamily: Fonts.SSPro_Regular,
          }}>
          {messageError || messageInfo}
        </Text>
      )}
      {!!messageInfo && !messageError && (
        <Text
          style={{
            fontSize: responsiveFont(11),
            color: Colors.grayLight,
            fontFamily: Fonts.SSPro_Regular,
          }}>
          {messageInfo}
        </Text>
      )}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textInputContainer: {
    paddingHorizontal: responsiveWidth(12),
    height: responsiveHeight(40),
    minHeight: 40,
    borderRadius: 5,
    marginBottom: 10,
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.darkLight,
  },
  textInput: {
    flex: 1,
    height: '100%',
    color: Colors.gray,
    paddingBottom: Platform.OS === 'ios' ? 0 : 5,
    textAlignVertical: 'center',
    // fontFamily: Fonts.nunitoRegular,
    fontSize: responsiveFont(16),
  },
  textStyle: {
    fontSize: responsiveFont(15),
    // fontFamily: Fonts.nunitoRegular,
    color: Colors.gray,
  },
});
