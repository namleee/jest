/**
 * Created by Hong HP on 11/17/19.
 */

import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import RouteKey from './RouteKey';
import Colors from '../Themes/Colors';
import Images from '../Themes/Images';
import {isIOS, responsiveFont, responsiveHeight} from '../Themes/Metrics';
import Fonts from '../Themes/Fonts';
import {SafeAreaView} from 'react-native-safe-area-context';

function CustomTabBar(props) {
  const {navigation, state} = props;
  const selectedTabIndex = state.index;

  function renderItem({route, custom, selected, icon, iconSelected, title}) {
    return (
      <TouchableOpacity
        key={title}
        style={styles.itemContainer}
        onPress={() => {
          if (route) {
            navigation.navigate(route);
          }
        }}>
        {icon && (
          <Image
            source={icon}
            style={{
              width: 20,
              height: 20,
              tintColor: selected ? Colors.primary : Colors.gray,
            }}
            resizeMode={'contain'}
          />
        )}
        <Text
          style={{
            marginTop: 3,
            color: selected ? Colors.primary : '#8F9BA9',
            fontSize: responsiveFont(14),
            fontFamily: Fonts.SSPro_Regular,
          }}>
          {title}
        </Text>
        {custom}
      </TouchableOpacity>
    );
  }

  return (
    <SafeAreaView edges={['bottom', 'left', 'right']} style={styles.container}>
      <View style={styles.wrapper}>
        {renderItem({
          route: RouteKey.HomeScreen,
          selected: selectedTabIndex === 0,
          icon: Images.customer,
          title: 'Home',
        })}
      </View>
    </SafeAreaView>
  );
}

export default CustomTabBar;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.darkLight,
    borderTopWidth: StyleSheet.hairlineWidth,
    paddingVertical: responsiveHeight(!isIOS() ? 5 : 0),
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: isIOS() ? 8 : 0,
  },
  itemContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
