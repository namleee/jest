/**
 * Created by Hong HP on 2/24/20.
 */
import {NavigationContainer, DarkTheme} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import RouteKey from './RouteKey';
import {connect} from 'react-redux';
import LoginScreen from '../Components/AuthComponent/LoginScreen';
import SplashScreen from '../Components/SplashScreen';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import CustomTabBar from './CustomTabBar';
import {navigationRef} from './NavigationService';
import Colors from '../Themes/Colors';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function AuthStackNavigation() {
  return (
    <Stack.Navigator headerMode={'none'}>
      <Stack.Screen name={RouteKey.LoginScreen} component={LoginScreen} />
    </Stack.Navigator>
  );
}

function HomeStackNavigation() {
  return (
    <Stack.Navigator headerMode={'none'}>
      <Stack.Screen name={RouteKey.LoginScreen} component={LoginScreen} />
    </Stack.Navigator>
  );
}

function MainTabNavigation() {
  return (
    <Tab.Navigator
      tabBar={(props) => <CustomTabBar {...props} />}
      tabBarOptions={{
        tabStyle: {
          backgroundColor: Colors.darkLight,
        },
      }}
      shifting={true}
      sceneAnimationEnabled={false}
      lazy>
      <Tab.Screen name={RouteKey.HomeScreen} component={HomeStackNavigation} />
    </Tab.Navigator>
  );
}

function MainStackNavigation() {
  return (
    <Stack.Navigator
      headerMode={'none'}
      screenOptions={{gestureEnabled: false}}>
      <Stack.Screen name={RouteKey.MainTab} component={MainTabNavigation} />
      <Stack.Screen name={RouteKey.HomeStack} component={HomeStackNavigation} />
    </Stack.Navigator>
  );
}

function AppNavigation(props) {
  function renderStack() {
    switch (props.appStack) {
      case RouteKey.MainStack:
        return <MainStackNavigation />;
      default:
        return <AuthStackNavigation />;
    }
  }

  return (
    <NavigationContainer theme={DarkTheme} ref={navigationRef}>
      {renderStack()}
    </NavigationContainer>
  );
}

export default connect((state) => ({
  appStack: state.app.appStack,
}))(AppNavigation);
