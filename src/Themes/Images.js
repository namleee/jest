/**
 * Created by Hong HP on 3/12/20.
 */
export default {
  logo: require('../assets/images/sts_demo_logo.png'),
  success: require('../assets/images/success.png'),
  checkBox: require('../assets/images/uncheck.png'),
  checkedBox: require('../assets/images/check.png'),
  addImage: require('../assets/images/add_image.png'),
  customer: require('../assets/images/customer.png'),
  settings: require('../assets/images/settings.png'),
  profile: require('../assets/images/profile.png'),
  appearance: require('../assets/images/appearance.png'),
  question: require('../assets/images/question.png'),
  info: require('../assets/images/info.png'),
  logout: require('../assets/images/logout.png'),
  bottomTab: require('../assets/images/bottom_tab.png'),
  background: require('../assets/images/background.png'),
  visa: require('../assets/images/visa.png'),
  masterCard: require('../assets/images/mastercard.png'),
  americanExpress: require('../assets/images/americanexpress.png'),
  paypal: require('../assets/images/paypalcard_icon.png'),
}
