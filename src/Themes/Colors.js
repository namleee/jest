/**
 * Created by Hong HP on 3/10/20.
 */
export default {
  primary: '#5EB6FE',
  secondary: '#FFF676',
  yellowLight: 'rgba(255,233,66,0.2)',
  grayLight: '#858688',
  black: '#000',
  red: '#ff0000',
  white: '#fff',
  background: '#F6F7FA',
  shamrock: '#2CCB8D',
  violet: '#5B388C',
  blackText: '#151B22',
  dark: '#181D2B',
  darkLight: '#313748',
  darkBold: '#0C101B',
  gray: '#A2ABB8',
}
