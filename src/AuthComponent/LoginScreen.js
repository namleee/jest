/**
 * Created by Hong HP on 3/9/20.
 */

import React, {useState, useEffect} from 'react';
import TextInputView from '../../common/TextInputView';
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import {useDispatch, connect} from 'react-redux';

import Colors from '../../Themes/Colors';
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics';
import ButtonView from '../../common/ButtonView';
import RouteKey from '../../navigation/RouteKey';

const {width, height} = Dimensions.get('window');
const imageRatio = 1125 / 1080;

function LoginScreen(props) {
  const [username, setUsername] = useState(__DEV__ ? 'nam22@yopmail.com' : '');
  const [password, setPassword] = useState(__DEV__ ? '1' : '');
  const [type, setType] = useState('');
  const dispatch = useDispatch();

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.dark}}>
      <ScrollView contentContainerStyle={{flexGrow: 1, paddingTop: 100}}>
        <View style={styles.container}>
          <TextInputView
            testID={'username_test'}
            placeholder={'Username'}
            value={username}
            onChangeText={(text) => {
              setUsername(text);
            }}
          />
          <TextInputView
            testID={'password_test'}
            placeholder={'Password'}
            value={password}
            secureTextEntry={true}
            onChangeText={(text) => {
              setPassword(text);
            }}
          />
          <Text
            style={styles.forgotPass}
            onPress={() => props.navigation.navigate(RouteKey.ForgotPassword)}>
            Forgot Password?
          </Text>
          <ButtonView
            testID={'login_button'}
            title={'Login'}
            style={{
              backgroundColor: Colors.darkLight,
              marginVertical: responsiveHeight(10),
            }}
            textStyle={{color: Colors.white}}
            onPress={handleUserLogin}
          />
          <Text style={styles.dontHaveAccount}>
            Don’t have an account yet?{' '}
            <Text
              style={{color: Colors.primary}}
              onPress={() => {
                props.navigation.navigate(RouteKey.SignUpScreen);
              }}>
              Sign Up
            </Text>
          </Text>
          <View style={styles.line} />
          <Text style={styles.or}>OR</Text>
          <ButtonView
            title={'Login with faceID'}
            style={{
              backgroundColor: Colors.darkLight,
              marginTop: responsiveHeight(15),
            }}
            textStyle={{color: Colors.white}}
            onPress={() => loginWithFinger()}
          />
          <ButtonView
            title={'Sign in with Google'}
            style={{
              backgroundColor: Colors.darkLight,
              marginVertical: responsiveHeight(10),
            }}
            textStyle={{color: Colors.white}}
            onPress={() => props.userLoginWithSocial('google')}
          />
          <ButtonView
            title={'Sign in with Facebook'}
            style={{
              backgroundColor: Colors.darkLight,
              marginBottom: responsiveHeight(10),
            }}
            textStyle={{color: Colors.white}}
            onPress={() => props.userLoginWithSocial('facebook')}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.dark,
    width,
    paddingHorizontal: 15,
  },
  contentWrapper: {
    flex: 2,
    paddingHorizontal: responsiveWidth(40),
    backgroundColor: Colors.white,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  forgotPass: {
    color: Colors.primary,
    alignSelf: 'flex-end',
    marginBottom: responsiveHeight(20),
  },
  dontHaveAccount: {
    color: Colors.gray,
    textAlign: 'center',
    marginTop: responsiveHeight(8),
  },
  logo: {
    width: width,
    height: responsiveHeight(500),
  },
  or: {
    alignSelf: 'center',
    marginTop: -10,
    backgroundColor: Colors.dark,
    color: Colors.gray,
    paddingHorizontal: 5,
  },
  line: {
    borderBottomWidth: 0.5,
    borderColor: Colors.darkLight,
    marginTop: 30,
  },
});

export default connect(null, (dispatch) => ({
  userLogin: (username, password, type) =>
    dispatch(userLogin(username, password, type)),
  userLoginWithSocial: (provider) => dispatch(userLoginWithSocial(provider)),
}))(LoginScreen);
